A basic command-based program written in JS that uses BitBucket's API to review pull requests.  You can:

* View a list of open pull requests
* See the details of a pull request
* View a diff of the pull request in your own configured graphical diff program
* Merge, approve, or decline requests

## Installation

From a git bash prompt:

First, set up your username/password:

```
git config --global bitbucket.credentials $(echo -n myBitbucketUsername:myBitbucketPassword | openssl enc -base64)
```

(this saves your username/password, base64 encoded, in your %userprofile%\\.gitconfig file)

Next, download it by running: 

```
curl https://bitbucket.org/dfalk/pullrequestreview/raw/master/git-review > /bin/git-review 
```

Running this command at any time will also update to the most recent version.

And finally just make sure you've got a diff tool configured.

## Configuring a diff tool

### Beyond Compare 3
```
git config --global diff.tool bc3
git config --global difftool.bc3.path "c:/program files (x86)/beyond compare 3/bcomp.exe"
```

### WinMerge
```
git config --global diff.tool winmerge
git config --global difftool.winmerge.cmd '"C:/Program Files (x86)/WinMerge/WinMergeU.exe" -e -u -r -dl Ours -dr Theirs "$LOCAL" "$REMOTE"'
```

## Usage

List available pull requests:

```
git review
```

Review a pull request:

```
git review [number of the pull request]
```

Approve:

```
git review [number of the pull request] approve
```

Merge:

```
git review [number of the pull request] merge
```
